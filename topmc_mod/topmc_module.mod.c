#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xe507201f, "module_layout" },
	{ 0x115eacfd, "cpu_online_mask" },
	{ 0xa94d7246, "boot_cpu_data" },
	{ 0xc0a3d105, "find_next_bit" },
	{ 0x365908a2, "remove_proc_entry" },
	{ 0x167e7f9d, "__get_user_1" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x6263e02d, "strncpy_from_user" },
	{ 0x9629486a, "per_cpu__cpu_number" },
	{ 0xfe7c4287, "nr_cpu_ids" },
	{ 0xde0bdcff, "memset" },
	{ 0xc8f0195d, "proc_mkdir" },
	{ 0x8d3894f2, "_ctype" },
	{ 0xea147363, "printk" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x57adf756, "per_cpu__this_cpu_off" },
	{ 0x56f494e0, "smp_call_function" },
	{ 0x13d38903, "create_proc_entry" },
	{ 0x5a18ea4a, "per_cpu__cpu_info" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "00CA09B40D9E32C1EC5796D");
