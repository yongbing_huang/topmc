#coding: UTF8

"""
Automatic run the application and topmc profiling 
"""

"""
Author: huangyongbing
Time: 2010.5.26
"""

import sys, os, time, re
import thread

parsec_apps = ["blackscholes",
		"bodytrack",
		"facesim",
		"ferret",
		"swaptions",
		"fluidanimate",
		"vips",
		"x264",
		"canneal",
		"dedup",
		"streamcluster"]

def run_topmc(name, interval, log):
	print time.time()
	print "in: run_topmc"
	
	time_curr = time.time()
	os.system("sh set_event_nehelam.sh")
        cmd = "python record_result.py " + name
        os.system(cmd)

	print "out: run_topmc"

def stop_topmc():
	print time.time()
	print "in: stop_topmc"

	kill_collect_trace()
	os.system("./topmc_trace/stop_trace 0")
	os.system("sh ./topmc_script/stop_profile.sh")

	print "out: stop_topmc"


def kill_app1():                
        print time.time()
        print "in: kill_app1"
        thread_id = '0'
        os.system("ps -ef | grep record_result > tmp.log")
        fd = open("tmp.log", 'r')
        lines = fd.readlines()
        for line in lines:
                tmpline = line.split(" ")
                if "python" in tmpline:
                        index = 1
                        for tmp in tmpline[1:]:
                                if tmp!='':
                                        break
                                index = index + 1
                        thread_id =  tmpline[index]
                        break
        fd.close()
        os.system("rm -f tmp.log")
        cmd = "kill -9 " + thread_id
        print cmd
        os.system(cmd)
        print time.time()

def run_app1(name, scale):
	print time.time()
	print "in: " + name + ":" + scale
	cmd = "taskset -c 1,2,3,4 parsecmgmt -a run -p " + name + " -i " + scale + " -c gcc-pthreads -n 4"
	print cmd
	os.system(cmd)
	print "out: run_app1" + name + ":" + scale

def change_event(event0,event1,event2,event3,log):	
	event_str0 = "incore_counter0_event";
	event_str1 = "incore_counter1_event";
	event_str2 = "incore_counter2_event";
	event_str3 = "incore_counter3_event";
	
	event_incore0 = event_str0 + "='" + event0 + "'";
	event_incore1 = event_str1 + "='" + event1 + "'";
	event_incore2 = event_str2 + "='" + event2 + "'";
	event_incore3 = event_str3 + "='" + event3 + "'";
	
	log.write(event_incore0 + '\n');
	log.write(event_incore1 + '\n');
	log.write(event_incore2 + '\n');
	log.write(event_incore3 + '\n');
	
	filecontent = [] 
	fileread = open('./topmc_script/set_event_nehelam.sh', 'r');
	for line in fileread.readlines():
		if (re.search("^" + event_str0, line)):
			strs = str(re.sub(r'incore_counter0_event=(.*)', event_incore0, line));
			filecontent.append(strs);
			continue;
		if (re.search("^" + event_str1, line)):
			strs = str(re.sub(r'incore_counter1_event=(.*)', event_incore1, line));
			filecontent.append(strs);
			continue;
		if (re.search("^" + event_str2, line)):
			strs = str(re.sub(r'incore_counter2_event=(.*)', event_incore2, line));
			filecontent.append(strs);
			continue;
		if (re.search("^" + event_str3, line)):
			strs = str(re.sub(r'incore_counter3_event=(.*)', event_incore3, line));
			filecontent.append(strs);
			continue;
		filecontent.append(line);
	fileread.close();

	os.remove('./topmc_script/set_event_nehelam.sh');
	filewrite = open('./topmc_script/set_event_nehelam.sh', 'w');
	for line in filecontent: 
		filewrite.write(line);
	filewrite.close();


def run_profile(seq_num, event0, event1, event2, event3, name, scale, thread_log):
        print time.time()
        print "Start: run_profile"

	thread_log.write("*********")
        thread_log.write("Run_profile" + str(seq_num)  + " " + name + " Start..."  + "*********\n")
	#change_event(event0, event1, event2, event3, thread_log);

        thread.start_new_thread(run_topmc,(name,"2",thread_log,))

	thread.start_new_thread(get_app_pid, (name,thread_log,))
        run_app1(name, scale)

        stop_topmc()
	thread_log.write("*********")
        thread_log.write("Run_profile" + str(seq_num) +  " Stop."  + "*********\n")
        os.system("echo '3' >/proc/sys/vm/drop_caches")
        time.sleep(30)
        print "Stop: run_profile"

if __name__ == "__main__":

	print "Topmc Profiling Starting..."

	time_curr = str(int(time.time()));
        thread_log = "./data/thread_" + time_curr + ".log"
        f = open(thread_log,'w')
	i = 0
	while i < 11:
		run_profile(1,"3c000003","c0010003","17010003","1e010003",parsec_apps[i], "simlarge",f)

	f.close()
	print "Topmc Profiling Ending."
