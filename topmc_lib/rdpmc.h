#ifndef _TOPMC_RDPMC_H_
#define _TOPMC_RDPMC_H_

//test overhead
//for READ_TSC: insn overhead 5, data store overhead 2
//for READ_PMC: insn overhead 6, data store overhead 2

// ----------------------------------------------------------------------------
#define SERIAL              do { __asm__ __volatile__ ("cpuid \n\t" ::: "ax", "bx", "cx", "dx"); } while(0) 
#define RDTSC(lo, hi)       do { __asm__ __volatile__ ("rdtsc \n\t" :"=a"(lo), "=d"(hi)); } while(0)
#define RDPMC(cnt, lo, hi)  do { __asm__ __volatile__ ("rdpmc \n\t" : "=a"(lo), "=d"(hi) : "c"(cnt)); } while(0) 

#define READ_TSC(l, h)      do { SERIAL; RDTSC(*l, *h);    SERIAL; } while(0)
#define READ_PMC(c, l, h)   do { SERIAL; RDPMC(c, *l, *h); SERIAL; } while(0)


#define CLFLUSH(addr) do {                  \
    SERIAL;                                 \
    __asm__ __volatile__ (                  \
            "mfence     \n\t"               \
            "clflush %0 \n\t"               \
            "mfence     \n\t"               \
            :                               \
            :"m"(addr)                      \
            );                              \
    SERIAL;                                 \
} while(0)

// ----------------------------------------------------------------------------
inline void my_void_print(char * fmt, ...) {} 
#define my_trace my_void_print
#define PRINT_S  printf
#define PRINT_V  my_void_print 

// TSC ------------------------------------------------------------------------
#define DEFINE_TSC     static unsigned int tsc_sl, tsc_sh, tsc_el, tsc_eh;
#define BEGIN_TSC      do{ READ_TSC(&tsc_sl, &tsc_sh); }while(0)
#define END_TSC 	   do{ READ_TSC(&tsc_el, &tsc_eh); }while(0)

#define PRINT_TSC \
	PRINT_V("TSC   start: low %9d, high %9d\n", tsc_sl, tsc_sh); \
	PRINT_V("TSC     end: low %9d, high %9d\n", tsc_el, tsc_eh); \
	PRINT_S("TSC  : %9d\n",(tsc_el-tsc_sl) + (tsc_eh-tsc_sh)*0xFFFFFFFF); \

// PMC ------------------------------------------------------------------------
#define DEFINE_COUNTER                                                      \
	static unsigned int counter0_sl, counter0_sh, counter0_el, counter0_eh; \
	static unsigned int counter1_sl, counter1_sh, counter1_el, counter1_eh; 

#define BEGIN_COUNTER do {                   \
	READ_PMC(0, &counter0_sl, &counter0_sh); \
	READ_PMC(1, &counter1_sl, &counter1_sh); \
} while(0)
	
#define END_COUNTER do {                     \
	READ_PMC(1, &counter1_el, &counter1_eh); \
	READ_PMC(0, &counter0_el, &counter0_eh); \
} while(0) 

#define COUNTER0_VALUE   ((counter0_el-counter0_sl)+(counter0_eh-counter0_sh)*0xFFFFFFFF) 
#define COUNTER1_VALUE   ((counter1_el-counter1_sl)+(counter1_eh-counter1_sh)*0xFFFFFFFF) 

#define PRINT_COUNTER do {                                                     \
    PRINT_V("PMC %d start: low %9d, high %9d\n", 0, counter0_sl, counter0_sh); \
    PRINT_V("PMC %d   end: low %9d, high %9d\n", 0, counter0_el, counter0_eh); \
    PRINT_S("PMC %d: %9d\n",  0, COUNTER0_VALUE);                              \
    PRINT_V("PMC %d start: low %9d, high %9d\n", 1, counter1_sl, counter1_sh); \
    PRINT_V("PMC %d   end: low %9d, high %9d\n", 1, counter1_el, counter1_eh); \
    PRINT_S("PMC %d: %9d\n",  1, COUNTER1_VALUE);                              \
} while(0)

#endif
