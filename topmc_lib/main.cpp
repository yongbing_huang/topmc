#include <iostream>
using namespace std;

#include "config.h"
#include "utils.h"
#include "format.h"
#include "convert.h"
#include "spmv.h"

#include "rdpmc.h" 
DEFINE_TSC         /* topmc */
DEFINE_COUNTER     /* topmc */

void usage(int argc, char* argv[]) {
    cout << "Usage:" << argv[0] << " matrix type iter" << endl;
    exit(0);
}

int main(int argc, char *argv[])
{ 
    if(argc < 4) usage(argc, argv); 

    GET_ARGS;
    READ_A;  
    vector x(A.get_n());
    x.init_rand(-1.0, 1.0); 
    vector y(A.get_m());
    y.init_zero();

    printf("-------------------------------- iter %d ----------------------------------\n", -1);
    BEGIN_TSC;                            /* topmc */
    BEGIN_COUNTER;                        /* topmc */
    mv(A, y, x); // warm up               
    END_COUNTER;                          /* topmc */
    END_TSC;                              /* topmc */
                                          
    PRINT_TSC;                            /* topmc */
    PRINT_COUNTER;                        /* topmc */
    printf("%7.3f%%\n", ((float)COUNTER0_VALUE)/(COUNTER1_VALUE)*100); 

    //printf("1: timer: %7d \t counter0: %7d \t counter 1: %7d \n", COUNTER0_VALUE, COUNTER1_VALUE); 

    double t = get_seconds();
    for (int i = 0; i < iter; i++) {
        printf("-------------------------------- iter %d ----------------------------------\n", i);
        BEGIN_TSC;                         /* topmc */
        BEGIN_COUNTER;                     /* topmc */
        mv(A, y, x);
        END_COUNTER;                       /* topmc */
        END_TSC;                           /* topmc */

        PRINT_TSC;                         /* topmc */
        PRINT_COUNTER;                     /* topmc */
        printf("%7.3f%%\n", ((float)COUNTER0_VALUE)/(COUNTER1_VALUE)*100); 
    }
    t = get_seconds() - t;

    double t1 = get_seconds();
    for (int i = 0; i < iter; i++) {
        printf("------------ loop overhead ----- iter %d ----------------------------------\n", i);
        BEGIN_TSC;                           /* topmc */
        BEGIN_COUNTER;                       /* topmc */
        //
        END_COUNTER;                         /* topmc */
        END_TSC;                             /* topmc */

        PRINT_TSC;                           /* topmc */
        PRINT_COUNTER;                       /* topmc */
    }
    t1 = get_seconds() - t1;

    t = t -t1;

    PRINT;
    return 0;
} 
